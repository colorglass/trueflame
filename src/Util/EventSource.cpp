#include <FDGE/Util/EventSource.h>

using namespace FDGE::Util::detail;
using namespace phmap;

EventSourceImpl::EventSourceImpl() noexcept {
}

EventSourceImpl::~EventSourceImpl() noexcept {
}

uint64_t EventSourceImpl::Register(std::function<void(const int&)> handler) {
    uint64_t id = _nextID.fetch_add(1);
    if (_handlers.emplace(id, std::move(handler)).second) {
        return id;
    }
    return 0;
}

uint64_t EventSourceImpl::LazyRegister(std::function<void(const int&)> handler) {
    uint64_t id = _nextID.fetch_add(1);
    std::unique_lock lock(_queuedRegistrations);
    _queuedRegistrations.emplace(id, std::move(handler));
    return id;
}

void EventSourceImpl::CommitLazyRegistrations() {
    std::unique_lock lock(_queuedRegistrations);
    while (!_queuedRegistrations.empty()) {
        auto& entry = _queuedRegistrations.front();
        _handlers.try_emplace(entry.first, std::move(entry.second));
        _queuedRegistrations.pop();
    }
}

bool EventSourceImpl::Unregister(uint64_t id) {
    return _handlers.erase(id);
}

void EventSourceImpl::Emit(const int& event) {
    for (auto& entry : _handlers) {
        entry.second(event);
    }
}
