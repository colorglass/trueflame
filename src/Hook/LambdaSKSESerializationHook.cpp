#include <FDGE/Hook/LambdaSKSESerializationHook.h>

using namespace FDGE::Hook;

void LambdaSKSESerializationHook::OnRevert() {
    _revertCallback();
}

void LambdaSKSESerializationHook::OnGameSaved(std::ostream& out) {
    _saveCallback(out);
}

void LambdaSKSESerializationHook::OnGameLoaded(std::istream& in) {
    _loadCallback(in);
}
