#include <FDGE/Logger.h>
#include <FDGE/Plugin.h>
#include <winerror.h>

using namespace FDGE;
using namespace FDGE::detail;
using namespace SKSE;

EXTERN_C IMAGE_DOS_HEADER __ImageBase;

namespace {
    template <class F>
    struct Callback {
        std::function<F> Function;
        uint64_t Priority;

        Callback(std::function<F> function, uint64_t priority) noexcept
            : Function(std::move(function)), Priority(priority) {}

        [[nodiscard]] inline auto operator<=>(const Callback& other) const noexcept {
            return Priority <=> other.Priority;
        }
    };

    [[nodiscard]] auto& GetPreInitializationCallbacks() {
        using value_type = Callback<void(const SKSE::LoadInterface&)>;
        static std::stack<value_type> callbacks;
        return callbacks;
    }

    [[nodiscard]] auto& GetInitializationCallbacks() {
        using value_type = Callback<void(const SKSE::LoadInterface&)>;
        static std::priority_queue<value_type, std::vector<value_type>, std::greater<value_type>> callbacks;
        return callbacks;
    }

    [[nodiscard]] auto& GetMessageCallbacks() {
        using value_type = Callback<void(const SKSE::MessagingInterface::Message&)>;
        static std::vector<value_type> callbacks;
        return callbacks;
    }
}  // namespace

LoadInitializer::LoadInitializer(std::function<void(const SKSE::LoadInterface&)> callback, uint64_t priority,
                                 bool isPre) noexcept {
    if (isPre) {
        GetInitializationCallbacks().emplace(std::move(callback), priority);
    } else {
        GetPreInitializationCallbacks().emplace(std::move(callback), priority);
    }
}

MessageHandler::MessageHandler(std::function<void(const SKSE::MessagingInterface::Message&)> callback,
                               uint64_t priority) noexcept {
    auto& queue = GetMessageCallbacks();
    queue.emplace_back(std::move(callback), priority);
    std::push_heap(queue.begin(), queue.end());
}

MessageHandler::MessageHandler(std::function<void(const SKSE::MessagingInterface::Message&)> callback,
                               uint32_t messageType, uint64_t priority) noexcept {
    auto& queue = GetMessageCallbacks();
    queue.emplace_back(std::move([&](const SKSE::MessagingInterface::Message& message) {
                           if (message.type == messageType) {
                               callback(message);
                           }
                       }), priority);
    std::push_heap(queue.begin(), queue.end());
}

namespace {
    [[nodiscard]] bool SKSEInit(const ::SKSE::LoadInterface& skse) {
        try {
            auto& preCallbacks = GetPreInitializationCallbacks();
            while (!preCallbacks.empty()) {
                preCallbacks.top().Function(skse);
                preCallbacks.pop();
            }
            Logger::Debug("Completed running all SKSE pre-initialization handlers.");

            auto plugin = Plugin::Lookup();
            if (plugin) {
                if (skse.IsEditor() && !plugin->SupportsCreationKit) {
                    Logger::Error("Plugin is not compatible with Creation Kit, it will not be loaded.");
                    return false;
                }
            }

            Logger::Debug("Module base address is 0x{:X}.", reinterpret_cast<uintptr_t>(&__ImageBase));

            Logger::Info("Initializing SKSE interfaces...");
            Init(&skse);
            Logger::Info("SKSE interfaces initialized.");

            if (plugin && plugin->UsesDeclarativeMessaging) {
                Logger::Info("Initializing SKSE messaging hooks...");
                SKSE::GetMessagingInterface()->RegisterListener([](SKSE::MessagingInterface::Message* msg) {
                    if (!msg) {
                        Logger::Error(
                            "Received a null SKSE message. This likely indicates a bug in SKSE or an installed plugin");
                        return;
                    }
                    Logger::Trace("Received SKSE message of type {}.", msg->type);
                    for (auto& callback : GetMessageCallbacks()) {
                        callback.Function(*msg);
                    }
                });
                Logger::Info("SKSE messaging hooks initialized.");
            }

            Logger::Debug("Beginning execution of SKSE initialization handlers.");
            auto& callbacks = GetInitializationCallbacks();
            while (!callbacks.empty()) {
                callbacks.top().Function(skse);
                callbacks.pop();
            }
            Logger::Debug("Completed running all SKSE initialization handlers.");

            Logger::Info("Plugin loaded successfully.");
            return true;
        } catch (const PluginIncompatible& e) {
            Logger::Error("Flagging plugin as incompatible: '{}'. This plugin will not be run.", e.what());
            return false;
        } catch (const std::exception& e) {
            Logger::Error("An error occurred during plugin initialization: '{}'. This plugin will not be run.",
                          e.what());
        } catch (...) {
            Logger::Error("An error occurred during plugin initialization. This plugin will not be run.");
        }
        RaiseException(ERROR_DLL_INIT_FAILED, 0, 0, nullptr);
        return false;
    }

    int ErrorHandler(unsigned int code, struct _EXCEPTION_POINTERS*) {
        Logger::Critical("System exception (code {}) raised during plugin initialization.", code);
        return EXCEPTION_CONTINUE_SEARCH;
    }
}  // namespace

bool FDGE::InitializeTrueflame(const ::SKSE::LoadInterface& skse) {
    __try {
        return SKSEInit(skse);
    } __except (ErrorHandler(GetExceptionCode(), GetExceptionInformation())) {
    }
    return false;
}

bool FDGE::InitializeTrueflame(const ::SKSE::LoadInterface* skse) { return InitializeTrueflame(*skse); }
