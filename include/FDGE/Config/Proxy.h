#pragma once

#include <filesystem>

#include <articuno/articuno.h>
#include <articuno/archives/ryml/ryml.h>

#include "../Util/EventSource.h"
#include "../Util/Maps.h"

namespace FDGE::Config {
    class DataRefreshingEvent {
    };

    class DataRefreshedEvent {
    };

    gluino_flags(ProxyOptions, uint32_t,
                 (RequireAtInitialization, 1 << 0),
                 (RequireContinuously, 1 << 1),
                 (DynamicFileExtensions, 1 << 2));

    gluino_enum(ArticunoFileType, uint8_t,
                (Dynamic, 0),
                (YAML, 1),
                (JSON, 2),
                (XML, 3),
                (INI, 4),
                (TOML, 5),
                (BSON, 6),
                (UBJSON, 7),
                (MessagePack, 8),
                (CBOR, 9),
                (IcyWind, 10));

    template <class Char = char, class Traits = std::char_traits<Char>, class Alloc = std::allocator<Char>,
            articuno::archive_flags Flags = articuno::archive_flags{}, auto Tag = ::articuno::default_tag{}>
    class ArticunoProxyProvider {
    public:
        using char_type = Char;
        using traits_type = Traits;
        using file_type = ArticunoFileType;

        template <class T>
        static void Read(std::basic_istream<Char, Traits>& input, file_type type, T& out) {
            switch (type.value) {
                case ArticunoFileType::enum_type::YAML:
                {
                    articuno::ryml::yaml_source<articuno::ryml::basic_ryml_parser<Char, Traits, Alloc>, Flags, Tag>
                            src(input);
                    src >> out;
                    break;
                }
//                case ArticunoFileType::enum_type::JSON:
//                {
//                    articuno::archives::jsoncons::jsoncons_iarchive<articuno::archives::jsoncons::json_parser<Char, Traits, Alloc>> archive(input);
//                    archive >> out;
//                    break;
//                }
//                case ArticunoFileType::enum_type::XML:
//                {
//                    articuno::archives::pugixml::pugixml_iarchive<articuno::archives::pugixml::xml_parser<Char, Traits, Alloc>> archive(input);
//                    archive >> out;
//                    break;
//                }
//                case ArticunoFileType::enum_type::INI:
//                case ArticunoFileType::enum_type::TOML:
//                {
//                    articuno::archives::toml11::toml11_iarchive<articuno::archives::toml11::toml_parser<Char, Traits, Alloc>> archive(input);
//                    archive >> out;
//                    break;
//                }
//                case ArticunoFileType::enum_type::BSON:
//                {
//                    articuno::archives::jsoncons::jsoncons_iarchive<articuno::archives::jsoncons::bson_parser<Char, Traits, Alloc>> archive(input);
//                    archive >> out;
//                    break;
//                }
//                case ArticunoFileType::enum_type::UBJSON:
//                {
//                    articuno::archives::jsoncons::jsoncons_iarchive<articuno::archives::jsoncons::ubjson_parser<Char, Traits, Alloc>> archive(input);
//                    archive >> out;
//                    break;
//                }
//                case ArticunoFileType::enum_type::MessagePack:
//                {
//                    articuno::archives::jsoncons::jsoncons_iarchive<articuno::archives::jsoncons::msgpack_parser<Char, Traits, Alloc>> archive(input);
//                    archive >> out;
//                    break;
//                }
//                case ArticunoFileType::enum_type::CBOR:
//                {
//                    articuno::archives::jsoncons::jsoncons_iarchive<articuno::archives::jsoncons::cbor_parser<Char, Traits, Alloc>> archive(input);
//                    archive >> out;
//                    break;
//                }
//                case ArticunoFileType::enum_type::IcyWind:
//                {
//                    // TODO: Add when IcyWind archive is done.
//                    break;
//                }
                default:
                    throw std::invalid_argument("Unknown file type.");
            }
        }

        [[nodiscard]] static std::optional<file_type> GetType(const std::filesystem::path& path) noexcept {
            auto extension = path.extension().generic_string();
            auto extensions = GetSupportedExtensions();
            auto result = extensions.find(extension);
            if (result == extensions.end()) {
                return {};
            }
            return result->second;
        }

        [[nodiscard]] static const Util::istr_flat_hash_map<std::string_view, file_type>& GetSupportedExtensions() noexcept {
            static Util::istr_flat_hash_map<std::string_view, file_type> supportedExtensions =
                    {{"yaml", file_type::YAML}, {"yml", file_type::YAML}, {"json", file_type::JSON},
                     {"xml", file_type::XML}, {"toml", file_type::TOML}, {"ini", file_type::TOML}};
            return supportedExtensions;
        }
    };

    template <class T>
    class Proxy : public Util::EventSource<DataRefreshedEvent>, public Util::EventSource<DataRefreshingEvent> {
    public:
        using config_type = T;

        virtual ~Proxy() noexcept = default;

        using Util::EventSource<DataRefreshingEvent>::Listen;
        using Util::EventSource<DataRefreshingEvent>::ListenForever;
        using Util::EventSource<DataRefreshedEvent>::Listen;
        using Util::EventSource<DataRefreshedEvent>::ListenForever;

        inline void Refresh() {
            DoRefresh();
        }

        virtual void StartWatching() const = 0;

        virtual void StopWatching() const = 0;

        virtual void Save() const = 0;

        [[nodiscard]] inline std::shared_ptr<T> Get() noexcept {
            return _data.load();
        }

        [[nodiscard]] inline std::shared_ptr<const T> Get() const noexcept {
            auto shared = _data.load();
            return *reinterpret_cast<const std::shared_ptr<const T>*>(&shared);
        }

        [[nodiscard]] inline T* operator->() noexcept {
            return _data.load().get();
        }

        [[nodiscard]] inline const T* operator->() const noexcept {
            return _data.load().get();
        }

        [[nodiscard]] inline T& operator*() noexcept {
            return *_data.load();
        }

        [[nodiscard]] inline const T& operator*() const noexcept {
            return *_data.load();
        }

        [[nodiscard]] inline ProxyOptions GetOptions() const noexcept {
            return _options;
        }

    protected:
        using Util::EventSource<DataRefreshingEvent>::Emit;
        using Util::EventSource<DataRefreshedEvent>::Emit;

        inline explicit Proxy(ProxyOptions options = {}) : _options(options), _data(std::make_shared<T>()) {
        }

        inline void DoRefresh() const {
            DataRefreshingEvent refreshing;
            DataRefreshedEvent refreshed;
            Emit(refreshing);
            Refresh(_initialized.exchange(true));
            Emit(refreshed);
        }

        virtual void Refresh(bool isReloading) const = 0;

        mutable std::atomic<std::shared_ptr<T>> _data;

    private:
        mutable std::atomic_bool _initialized;
        ProxyOptions _options;
    };
}
