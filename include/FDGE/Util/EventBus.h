#pragma once

#include "EventSource.h"

namespace FDGE::Util {
    template <class T>
    class __declspec(dllexport) EventBus : public EventSource<T> {
    public:
        inline void Emit(const T& event) const {
            EventSource<T>::Emit(event);
        }
    };
}
