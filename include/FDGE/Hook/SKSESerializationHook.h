#pragma once

namespace FDGE::Hook {
    class SKSESerializationHook {
    protected:
        explicit SKSESerializationHook(std::string_view name);

        explicit SKSESerializationHook(std::string&& name);

        virtual ~SKSESerializationHook() noexcept;

        virtual void OnRevert();

        virtual void OnGameSaved(std::ostream& out);

        virtual void OnGameLoaded(std::istream& in);

    private:
        void Initialize();

        std::string _name;
    };
}
