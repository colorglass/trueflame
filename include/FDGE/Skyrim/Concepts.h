#pragma once

#include <gluino/concepts.h>
#include <RE/Skyrim.h>

namespace FDGE::Skyrim {
    template <class T>
    concept Form = gluino::subtype_of<std::remove_pointer_t<T>, RE::TESForm>;

    template <class T>
    concept FormPointer = gluino::subtype_of<std::remove_pointer_t<T>, RE::TESForm> && std::is_pointer_v<T>;

    template <class T>
    concept TemplateForm = gluino::subtype_of<T, RE::TESActorBaseData> || gluino::subtype_of<T, RE::TESObjectWEAP> ||
            gluino::subtype_of<T, RE::TESObjectARMO>;

    template <class T>
    concept LeveledItemForm = gluino::subtype_of<T, RE::TESObjectWEAP> || gluino::subtype_of<T, RE::TESObjectARMO> ||
            gluino::subtype_of<T, RE::AlchemyItem> || gluino::subtype_of<T, RE::TESObjectLIGH> ||
            gluino::subtype_of<T, RE::TESObjectMISC> || gluino::subtype_of<T, RE::IngredientItem> ||
            gluino::subtype_of<T, RE::TESKey> || gluino::subtype_of<T, RE::TESAmmo> ||
            gluino::subtype_of<T, RE::TESObjectBOOK> || gluino::subtype_of<T, RE::ScrollItem> ||
            gluino::subtype_of<T, RE::TESSoulGem> || gluino::subtype_of<T, RE::BGSApparatus>;
}
