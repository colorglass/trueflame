# Trueflame
Trueflame is a static library component that is part of the Fully Dynamic Game Engine (FuDGE) project. It acts as a
companion to CommonLibSSE to provide more structured and efficient SKSE plugin structure that is more suitable to larger
projects. Trueflame plugins do not require the end-user of your plugin to have the Fully Dynamic Game Engine plugin
installed (for more advanced functionality that relies on Fully Dynamic Game Engine, see the companion Hopesfire
project).

## Getting Started
For a complete example, see the
[FuDGE sample plugin project](https://www.gitlab.com/colorglass/fudge-sample-project-cpp). This project is a more
complete demonstration and has more thorough documentation than you will see here and it is strongly recommended to go
through its source and documentation.

---

To use Trueflame, first add the custom Color-Glass Vcpkg repository to your project. Create a file in
the project root called `vcpkg-configuration.json` with the following content.

```json
{
    "registries": [
        {
            "kind": "git",
            "repository": "https://gitlab.com/colorglass/vcpkg-colorglass",
            "baseline": "02e6ce0d7dd8880fdbef555a510598cfc28fc986",
            "packages": [
                "articuno",
                "commonlibsse-ng",
                "gluino",
                "trueflame"
            ]
        }
    ]
}
```

To include the necessary dependencies, add them to your `vcpkg.json` file in the root of the project:

```json
{
    "$schema": "https://raw.githubusercontent.com/microsoft/vcpkg/master/scripts/vcpkg.schema.json",
    "name": "skyrim-ng-sample-plugin",
    "version-string": "1.0.0",
    "description": "Sample SKSE plugin based on Fully Dynamic Game Engine's platform.",
    "homepage": "https://www.skyrimng.com",
    "license": "Apache-2.0",
    "dependencies": [
        "trueflame"
    ]
}
```

Trueflame will transitively depend on [CommonLibSSE NG](http://www.github.com/CharmedBaryon/CommonLibSSE-NG) by default.
If you wish to use a different fork of CommonLibSSE, then you can replace `"trueflame"` with the following:

```json
{
    "name": "trueflame",
    "default-features": false
}
```

This will omit CommonLibSSE NG, but a CommonLibSSE library is still required. You can use the other ports in the
Color-Glass Vcpkg repository such as `commonlibsse`, `commonlibsse-po3-ae`, `commonlibsse-po3-se`, or `commonlibvr`, or
use a Git submodule to include CommonLibSSE as a sub-project.

### Defining a Plugin
In your `CMakeLists.txt`, you will need to locate the Trueflame package.

```cmake
find_package(Trueflame CONFIG REQUIRED)
```

You can now link a conventional shared library to Trueflame like so:

```cmake
include(Trueflame)

add_trueflame_plugin(${PROJECT_NAME}
    SOURCES
        ${headers}
        ${sources}
)
```

This will add a shared library target, and generate your `SKSEPlugin_Version`, `SKSEPlugin_Query`, and `SKSEPlugin_Load`
content, and enable the Trueflame loader (as a replacement for `SKSEPlugin_Load`). For more information on how to
define a plugin, [see the documentation](https://gitlab.com/colorglass/trueflame/-/wikis/Plugin-Declaration).

## Porting a Plugin
To port an existing plugin, you can add Trueflame to an existing project and use its library features without letting it
take over the plugin metadata and lifecycle.

```cmake
find_package(Trueflame CONFIG REQUIRED)

add_library(${PROJECT_NAME}
    SHARED
        ${headers}
        ${sources}
)

target_link_libraries(${PROJECT_NAME}
    PRIVATE
        FullyDynamicGameEngine::Trueflame
)
```

[See the documentation](https://gitlab.com/colorglass/trueflame/-/wikis/Plugin-Declaration) for information on how to
use Trueflame's plugin lifecycle if you wish to migrate your plugin to be fully structured by Trueflame. Migrations
can be done piecemeal by integrating Trueflame's lifecycle with yours in C++, defining it entirely within C++, and
finally with generated definitions via the CMake module.

