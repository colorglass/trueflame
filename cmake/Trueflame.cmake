function(trueflame_parse_version VERSION)
    message("${version_match_count}")
    string(REGEX MATCHALL "^([0-9]+)(\\.([0-9]+)(\\.([0-9]+)(\\.([0-9]+))?)?)?$" version_match "${VERSION}")
    unset(TRUEFLAME_VERSION_MAJOR PARENT_SCOPE)
    unset(TRUEFLAME_VERSION_MINOR PARENT_SCOPE)
    unset(TRUEFLAME_VERSION_PATCH PARENT_SCOPE)
    unset(TRUEFLAME_VERSION_TWEAK PARENT_SCOPE)

    if ("${version_match} " STREQUAL " ")
        set(TRUEFLAME_VERSION_MATCH FALSE PARENT_SCOPE)
        return()
    endif ()

    set(TRUEFLAME_VERSION_MATCH TRUE PARENT_SCOPE)
    set(TRUEFLAME_VERSION_MAJOR "${CMAKE_MATCH_1}" PARENT_SCOPE)
    set(TRUEFLAME_VERSION_MINOR "0" PARENT_SCOPE)
    set(TRUEFLAME_VERSION_PATCH "0" PARENT_SCOPE)
    set(TRUEFLAME_VERSION_TWEAK "0" PARENT_SCOPE)

    if (DEFINED CMAKE_MATCH_3)
        set(TRUEFLAME_VERSION_MINOR "${CMAKE_MATCH_3}" PARENT_SCOPE)
    endif ()
    if (DEFINED CMAKE_MATCH_5)
        set(TRUEFLAME_VERSION_PATCH "${CMAKE_MATCH_5}" PARENT_SCOPE)
    endif ()
    if (DEFINED CMAKE_MATCH_7)
        set(TRUEFLAME_VERSION_TWEAK "${CMAKE_MATCH_7}" PARENT_SCOPE)
    endif ()
endfunction()

function(enable_trueflame_loader TARGET)
    set(trueflame_loader_file "${CMAKE_CURRENT_BINARY_DIR}/__${TARGET}Loader.cpp")

    file(WRITE "${trueflame_loader_file}"
            "#include <FDGE/Plugin.h>\n"
            "\n"
            "EXTERN_C [[maybe_unused]] __declspec(dllexport) bool SKSEAPI\n"
            "        SKSEPlugin_Load(const ::SKSE::LoadInterface* skse) {\n"
            "    FDGE::InitializeTrueflame(skse);\n"
            "}\n")

    target_sources("${TARGET}" PRIVATE "${trueflame_loader_file}")
    target_compile_definitions("${TARGET}" PRIVATE __CMAKE_TRUEFLAME_LOADER=1)
    set_property(TARGET "${TARGET}"
            APPEND PROPERTY ADDITIONAL_CLEAN_FILES "${trueflame_loader_file}")
endfunction()

function(add_trueflame_plugin TARGET)
    set(options OPTIONAL USE_ADDRESS_LIBRARY USE_SIGNATURE_SCANNING SUPPORTS_EDITOR EXCLUDE_FROM_ALL
            NO_LOADER NO_MESSAGING)
    set(oneValueArgs NAME AUTHOR EMAIL VERSION MINIMUM_SKSE_VERSION PLUGIN_ID RECORD_ID)
    set(multiValueArgs COMPATIBLE_RUNTIMES SERDE_NAMES SOURCES)
    cmake_parse_arguments(PARSE_ARGV 1 ADD_TRUEFLAME_PLUGIN "${options}" "${oneValueArgs}"
            "${multiValueArgs}")

    set(trueflame_plugin_file "${CMAKE_CURRENT_BINARY_DIR}/__${TARGET}Plugin.cpp")

    # Set the plugin name.
    set(trueflame_plugin_name "${TARGET}")
    if (DEFINED ADD_TRUEFLAME_PLUGIN_NAME)
        set(trueflame_plugin_name "${ADD_TRUEFLAME_PLUGIN_NAME}")
    endif ()

    # Setup version number of the plugin.
    set(trueflame_plugin_version "${PROJECT_VERSION}")
    if (DEFINED ADD_TRUEFLAME_PLUGIN_VERSION)
        set(trueflame_plugin_version "ADD_TRUEFLAME_PLUGIN_NAME")
    endif ()
    trueflame_parse_version("${trueflame_plugin_version}")
    if (NOT DEFINED TRUEFLAME_VERSION_MAJOR)
        message(FATAL_ERROR "Unable to parse plugin version number ${trueflame_plugin_version}.")
    endif ()
    set(trueflame_plugin_version "REL::Version{ ${TRUEFLAME_VERSION_MAJOR}, ${TRUEFLAME_VERSION_MINOR}, ${TRUEFLAME_VERSION_PATCH}, ${TRUEFLAME_VERSION_TWEAK} }")

    # Handle minimum SKSE version constraints.
    if (NOT DEFINED ADD_TRUEFLAME_PLUGIN_MINIMUM_SKSE_VERSION)
        set(ADD_TRUEFLAME_PLUGIN_MINIMUM_SKSE_VERSION 0)
    endif ()
    trueflame_parse_version("${ADD_TRUEFLAME_PLUGIN_MINIMUM_SKSE_VERSION}")
    if (NOT TRUEFLAME_VERSION_MATCH)
        message(FATAL_ERROR "Unable to parse SKSE minimum SKSE version number "
                "${ADD_TRUEFLAME_PLUGIN_MINIMUM_SKSE_VERSION}.")
    endif ()
    set(trueflame_min_skse_version "REL::Version{ ${TRUEFLAME_VERSION_MAJOR}, ${TRUEFLAME_VERSION_MINOR}, ${TRUEFLAME_VERSION_PATCH}, ${TRUEFLAME_VERSION_TWEAK} }")

    # Setup compatibility configuration.
    if (NOT ADD_TRUEFLAME_PLUGIN_USE_SIGNATURE_SCANNING AND NOT DEFINED ADD_TRUEFLAME_PLUGIN_COMPATIBLE_RUNTIMES)
        set(ADD_TRUEFLAME_PLUGIN_USE_ADDRESS_LIBRARY TRUE)
    endif ()
    if (ADD_TRUEFLAME_PLUGIN_USE_ADDRESS_LIBRARY OR ADD_TRUEFLAME_PLUGIN_USE_SIGNATURE_SCANNING)
        if (DEFINED ADD_TRUEFLAME_PLUGIN_COMPATIBLE_RUNTIMES)
            message(FATAL_ERROR "COMPATIBLE_RUNTIMES option should not be used with USE_ADDRESS_LIBRARY or "
                    "USE_SIGNATURE_SCANNING")
        endif ()

        if (NOT ADD_TRUEFLAME_PLUGIN_USE_ADDRESS_LIBRARY)
            set(trueflame_plugin_compatibility "VersionIndependent::ViaSignatureScanning")
        elseif (NOT ADD_TRUEFLAME_PLUGIN_USE_SIGNATURE_SCANNING)
            set(trueflame_plugin_compatibility "VersionIndependent::ViaAddressLibrary")
        else ()
            set(trueflame_plugin_compatibility "VersionIndependent::ViaAddressLibraryAndSignatureScanning")
        endif ()
    else ()
        list(LENGTH ${ADD_TRUEFLAME_PLUGIN_COMPATIBLE_RUNTIMES} trueflame_plugin_compatibility_count)
        if(trueflame_plugin_compatibility_count GREATER 16)
            message(FATAL_ERROR "No more than 16 version numbers can be provided for COMPATIBLE_RUNTIMES.")
        endif()
        foreach (SKYRIM_VERSION ${ADD_TRUEFLAME_PLUGIN_COMPATIBLE_RUNTIMES})
            if (DEFINED trueflame_plugin_compatibility)
                set(trueflame_plugin_compatibility "${trueflame_plugin_compatibility}, ")
            endif ()
            trueflame_parse_version("${SKYRIM_VERSION}")
            if (NOT TRUEFLAME_VERSION_MATCH)
                message(FATAL_ERROR "Unable to parse Skyrim runtime version number ${SKYRIM_VERSION}.")
            endif ()
            set(trueflame_plugin_compatibility "${trueflame_plugin_compatibility}REL::Version{ ${TRUEFLAME_VERSION_MAJOR}, ${TRUEFLAME_VERSION_MINOR}, ${TRUEFLAME_VERSION_PATCH}, ${TRUEFLAME_VERSION_TWEAK} }")
        endforeach ()
        set(trueflame_plugin_compatibility "{ ${trueflame_plugin_compatibility} }")
    endif ()

    # Setup Trueflame's SKSE serialization hooks.
    if(NOT DEFINED ADD_TRUEFLAME_PLUGIN_PLUGIN_ID)
        set(ADD_TRUEFLAME_PLUGIN_PLUGIN_ID "")
    endif()
    string(LENGTH "${ADD_TRUEFLAME_PLUGIN_PLUGIN_ID}" trueflame_pluginid_length)
    if(${trueflame_pluginid_length} GREATER 4)
        message(FATAL_ERROR "The serialization plugin ID must have no more than four characters.")
    endif()
    if(NOT DEFINED ADD_TRUEFLAME_PLUGIN_RECORD_ID)
        set(ADD_TRUEFLAME_PLUGIN_RECORD_ID "SAVE")
    endif()
    string(LENGTH "${ADD_TRUEFLAME_PLUGIN_RECORD_ID}" trueflame_recordid_length)
    if(${trueflame_recordid_length} GREATER 4)
        message(FATAL_ERROR "The serialization record ID must have no more than four characters.")
    endif()

    # Handle extended features of Trueflame setup.
    if (ADD_TRUEFLAME_PLUGIN_NO_MESSAGING)
        set(trueflame_plugin_messaging "false")
    else ()
        set(trueflame_plugin_messaging "true")
        if (ADD_TRUEFLAME_PLUGIN_NO_LOADER)
            message(WARNING "Declarative messaging is being used (possibly due to being on by default) with NO_LOADER "
                    "specified. Declarative messaging requires using the Trueflame loader. It is recommended this be "
                    "done by letting CMake generate the loader. If you are not, you should add NO_MESSAGING.")
        endif ()
    endif ()
    if (ADD_TRUEFLAME_PLUGIN_SUPPORTS_EDITOR)
        set(trueflame_plugin_editor "true")
    else ()
        set(trueflame_plugin_editor "false")
        if (ADD_TRUEFLAME_PLUGIN_NO_LOADER)
            message(WARNING "Creation Kit (editor) support is off. Enforcement of this requires the Trueflame loader. "
                    "It is recommended this be done by letting CMake generate the loader; if you are not, you should "
                    "add SUPPORTS_EDITOR.")
        endif ()
    endif ()

    # Handle the list of serialization module names for use with FuDGE cosaves.
    if(NOT DEFINED ADD_TRUEFLAME_PLUGIN_SERDE_NAMES)
        set(ADD_TRUEFLAME_PLUGIN_SERDE_NAMES " ")
    endif()
    list(LENGTH ${ADD_TRUEFLAME_PLUGIN_SERDE_NAMES} trueflame_plugin_serde_name_count)
    if(trueflame_plugin_serde_name_count GREATER 16)
        message(FATAL_ERROR "No more than 16 names can be provided for SERDE_NAMES.")
    endif()
    if (trueflame_plugin_serde_name_count GREATER 0)
        foreach (SERDE_NAME ${ADD_TRUEFLAME_PLUGIN_SERDE_NAMES})
            if (DEFINED trueflame_plugin_serde_names)
                set(trueflame_plugin_serde_names "${trueflame_plugin_serde_names}, ")
            endif ()
            set(trueflame_plugin_serde_names "${trueflame_plugin_serde_names}\"${SERDE_NAME}\"")
        endforeach ()
    endif()

    file(WRITE "${trueflame_plugin_file}"
            "#include <FDGE/Plugin.h>\n"
            "\n"
            "TrueflamePlugin(\n"
            "    Version = ${trueflame_plugin_version};\n"
            "    Name = \"${trueflame_plugin_name}\";\n"
            "    Author = \"${ADD_TRUEFLAME_PLUGIN_AUTHOR}\";\n"
            "    Email = \"${ADD_TRUEFLAME_PLUGIN_EMAIL}\";\n"
            "    CompatibleSkyrimVersions = ${trueflame_plugin_compatibility};\n"
            "    SKSEMinimum = ${trueflame_min_skse_version};\n"
            "    SKSESerializationPluginID = \"${ADD_TRUEFLAME_PLUGIN_PLUGIN_ID}\";\n"
            "    SKSESerializationRecordID = \"${ADD_TRUEFLAME_PLUGIN_RECORD_ID}\";\n"
            "    SupportsCreationKit = ${trueflame_plugin_editor};\n"
            "    UsesDeclarativeMessaging = ${trueflame_plugin_messaging};\n"
            "    SerializationModuleNames = {${trueflame_plugin_serde_names}};\n"
            ")\n")

    add_library("${TARGET}" SHARED $<$<BOOL:${ADD_TRUEFLAME_PLUGIN_EXCLUDE_FROM_ALL}>:EXCLUDE_FROM_ALL>
            ${ADD_TRUEFLAME_PLUGIN_SOURCES})

    target_sources("${TARGET}" PRIVATE "${trueflame_plugin_file}")
    target_compile_definitions("${TARGET}" PRIVATE __CMAKE_TRUEFLAME_PLUGIN=1)
    target_link_libraries("${TARGET}" PRIVATE FullyDynamicGameEngine::Trueflame)
    set_property(TARGET "${TARGET}"
            APPEND PROPERTY ADDITIONAL_CLEAN_FILES "${trueflame_plugin_file}")

    if (NOT DEFINED ADD_TRUEFLAME_PLUGIN_NO_LOADER)
        enable_trueflame_loader("${TARGET}")
    endif ()
endfunction()
